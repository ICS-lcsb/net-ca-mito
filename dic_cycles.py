# Dictionary variables contains the index, name, min and max boundaries for all the dynamical variables of cortassa's model

variables = {
        "adpc":{
                "index" : 0,
                "name"  : r'$\mathrm{ADP_c~(mM)}$',
                "min"   : 0.,
                "max"   : 3.
        },
        "adpm":{
                "index" : 1,
                "name"  : r'$\mathrm{ADP_m~(mM)}$',
                "min"   : 0.,
                "max"   : 12.5
        },
        "akg":{
                "index" : 2,
                "name"  : r'$\mathrm{\alpha KG~(mM)}$',
                "min"   : 0.,
                "max"   : 0.2
        },
        "asp":{
                "index" : 3,
                "name"  : r'$\mathrm{ASP~(mM)}$',
                "min"   : 0.,
                "max"   : 1.
        },
        "atpc":{
                "index" : 4,
                "name"  : r'$\mathrm{ATP_c~(mM)}$',
                "min"   : 0.,
                "max"   : 3.
        },
        "atpm":{
                "index" : 5,
                "name"  : r'$\mathrm{ATP_m~(mM)}$',
                "min"   : 0.,
                "max"   : 1.5    #3.5
        },
        "cac":{
                "index" : 6,
                "name"  : r'$\mathrm{Ca_c~(\mu M)}$',
                "min"   : 0.,
                "max"   : 2.5
        },
        "cam":{
                "index" : 7,
                "name"  : r'$\mathrm{Ca_m~(\mu M)}$',
                "var"   : "mean_cam",
                "min"   : 0.,
                "max"   : 5.
        },
        "cit":{
                "index" : 8,
                "name"  : r'$\mathrm{CIT~(mM)}$',
                "min"   : 0.,
                "max"   : 1.
        },
        "fum":{
                "index" : 9,
                "name"  : r'$\mathrm{FUM~(mM)}$',
                "min"   : 0.,
                "max"   : 0.1
        },
        "isoc":{
                "index" : 10,
                "name"  : r'$\mathrm{ISOC~(mM)}$',
                "min"   : 0.,
                "max"   : 0.10
        },
        "mal":{
                "index" : 11,
                "name"  : r'$\mathrm{MAL~(mM)}$',
                "min"   : 0.,
                "max"   : 0.25
        },
        "nadhm":{
                "index" : 12,
                "name"  : r'$\mathrm{NADH_m~(mM)}$',
                "min"   : 0.,
                "max"   : 0.8
        },
        "nadm":{
                "index" : 13,
                "name"  : r'$\mathrm{NAD_m~(mM)}$',
                "min"   : 0.,
                "max"   : 0.8
        },
        "oaa":{
                "index" : 14,
                "name"  : r'$\mathrm{OAA~(mM)}$',
                "min"   : 0.,
                "max"   : 1.e-2
        },
        "psi":{
                "index" : 15,
                "name"  : r'$\mathrm{\Delta \Psi~(mV)}$',
                "min"   : 100., #155.,
                "max"   : 130.  #156.
        },
        "scoa":{
                "index" : 16,
                "name"  : r'$\mathrm{SCoA~(mM)}$',
                "min"   : 0.,
                "max"   : 0.10
        },
        "suc":{
                "index" : 17,
                "name"  : r'$\mathrm{SUC~(mM)}$',
                "min"   : 0.,
                "max"   : 1.e-3
        }
}

processes = {
        "aco":{
                "index" : 0,
                "name"  : [r'$J_{aco}$', r'$\Delta G_{aco}$', r'$T \sigma_{aco}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "ant":{
                "index" : 1,
                "name"  : [r'$J_{ant}$', r'$\Delta G_{ant}$', r'$T \sigma_{ant}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "cs":{
                "index" : 2,
                "name"  : [r'$J_{cs}$', r'$\Delta G_{cs}$', r'$T \sigma_{cs}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "erout":{
                "index" : 3,
                "name"  : [r'$J_{erout}$', r'$\Delta G_{erout}$', r'$T \sigma_{erout}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "f1":{
                "index" : 4,
                "name"  : [r'$J_{f1}$', r'$\Delta G_{f1}$', r'$T \sigma_{f1}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "fh":{
                "index" : 5,
                "name"  : [r'$J_{fh}$', r'$\Delta G_{fh}$', r'$T \sigma_{fh}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "hl":{
                "index" : 6,
                "name"  : [r'$J_{hl}$', r'$\Delta G_{hl}$', r'$T \sigma_{hl}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "hyd":{
                "index" : 7,
                "name"  : [r'$J_{hyd}$', r'$\Delta G_{hyd}$', r'$T \sigma_{hyd}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "idh":{
                "index" : 8,
                "name"  : [r'$J_{idh}$', r'$\Delta G_{idh}$', r'$T \sigma_{idh}$'],
                "min"   : [0.068],      #[0.08],
                "max"   : [0.070]       #[0.12]
        },
        "kgdh":{
                "index" : 9,
                "name"  : [r'$J_{kgdh}$', r'$\Delta G_{kgdh}$', r'$T \sigma_{kgdh}$'],
                "min"   : [0.068],      #[0.08],
                "max"   : [0.070]       #[0.12]
        },
        "mdh":{
                "index" : 10,
                "name"  : [r'$J_{mdh}$', r'$\Delta G_{mdh}$', r'$T \sigma_{mdh}$'],
                "min"   : [0.068],      #[0.08],
                "max"   : [0.070]       #[0.12]
        },
        "ncx":{
                "index" : 11,
                "name"  : [r'$J_{ncx}$', r'$\Delta G_{ncx}$', r'$T \sigma_{ncx}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "o":{
                "index" : 12,
                "name"  : [r'$J_{o}$', r'$\Delta G_{o}$', r'$T \sigma_{o}$'],
                "min"   : [0.2067],     #[0.28],
                "max"   : [0.2087]      #[0.32]
        },
        "sdh":{
                "index" : 13,
                "name"  : [r'$J_{sdh}$', r'$\Delta G_{sdh}$', r'$T \sigma_{sdh}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "serca":{
                "index" : 14,
                "name"  : [r'$J_{serca}$', r'$\Delta G_{serca}$', r'$T \sigma_{serca}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "sl":{
                "index" : 15,
                "name"  : [r'$J_{sl}$', r'$\Delta G_{sl}$', r'$T \sigma_{sl}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "uni":{
                "index" : 16,
                "name"  : [r'$J_{uni}$', r'$\Delta G_{uni}$', r'$T \sigma_{uni}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "tca":{
                "index" : 17,
                "name"  : [r'$J_{cs}$', r'$\Delta G_{tca}$', r'$T \sigma_{tca}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "cF1":{
                "index" : 18,
                "name"  : [r'$\psi_{F1}$', r'$\Delta G_{F1}$', r'$T \sigma_{F1}$'],
                "min"   : [0.],
                "max"   : [0.10]
        },
        "cOXTCA":{
                "index" : 19,
                "name"  : [r'$\psi_{OXTCA}$', r'$\Delta G_{OXTCA}$', r'$T \sigma_{OXTCA}$'],
                "min"   : [0.],
                "max"   : [0.10]
        }
}

