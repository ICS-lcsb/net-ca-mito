# HOW TO RUN THE CODE
# python3 ThermoMitoCa_uncoupled.py <ip3> <accoa> <path_output> <vserca>
#
# HOW TO ACCESS DATA (python command line or python script)
# data=np.load("ip3_X.XX_accoa_X.XXeXXX_test.npy", allow_pickle=True).item()
# data.t         -> time points
# data.y         -> concentrations of all species at each time point
# data.flux      -> fluxes of all processes at each time point
# data.DG        -> \Delta_r G of all processes at each time point
# data.EPR       -> EPR (T*\sigma) of all processes at each time point
# data.ncw       -> Nonconservative work for "r1out", "r1in" and "r2out"
# -----------------------------------------------------------------------------
# INDICES
#       SPECIES:                    |   PROCESSES
#       0:adpc          10:isoc     |   0:aco           10:mdh
#       1:adpm          11:mal      |   1:ant           11:ncx
#       2:akg           12:nadhm    |   2:cs            12:ox
#       3:asp           13:nadm     |   3:erout (ip3)   13:sdh
#       4:atpc          14:oaa      |   4:f1            14:serca
#       5:atpm          15:psi      |   5:fh            15:sl
#       6:cac           16:scoa     |   6:hl            16:uni
#       7:cam           17:suc      |   7:hyd           17:(leak)
#       8:cit                       |   8:idh           
#       9:fum                       |   9:kgdh          
# -----------------------------------------------------------------------------

import sys, os
os.environ['OMP_NUM_THREADS']='1'
import numpy as np
import gc
from scipy.signal import find_peaks, peak_prominences
import matplotlib.pyplot as plt

from scipy.integrate import solve_ivp

# Parameters entered in command line
ip3 = float(sys.argv[1])                # in uM                
accoa = float(sys.argv[2])              # in mM
run = sys.argv[3]                       # 'XXX/' = subfolder where output files are stored
vserca = float(sys.argv[4])

# ---------------------------------------------------------------------------------------------
# Name of the output file
foldername = run
filename = "%sip3_%.6f_accoa_%.2e.npy" % (foldername, ip3, accoa)

if os.path.isfile("%s" % (filename)): os.remove("%s" % (filename))
if os.path.isfile("%s.png" % (filename[:-4])): os.remove("%s.png" % (filename[:-4]))

def fluxes(v):
        # Fluxes are all returned in mM/s
        # For convenience of notation
        [adpc, adpm, akg, asp, atpc, atpm, cac, cam, cit, fum, isoc, mal, nadhm, nadm, oaa, psi, scoa, suc]=v
        caer = coef1 - cac * coef2 - cam * coef3

        # Aconitase
        Jaco = kfaco * (cit -isoc/keaco)
        
        # Exchange of mitochondrial ATP for cytosolic ADP (antiporter)
        Jant = vant * (1. - alphac * atpc * adpm * np.exp(-psi / RTF) / (alpham * adpc * atpm)) / ((1. + alphac * atpc * np.exp(-f * psi / RTF)/ adpc) * (1. + adpm / (alpham * atpm)))
        
        # Citrate synthase (CS)
        Jcs = Vmaxcs * accoa / (accoa + kmaccoa + (kmoaa* accoa /oaa)*(1.+accoa/kiaccoa) + (ksaccoa * kmoaa)/oaa)        # Dudycha
        
        # Ca2+ release from ER via IP3Rs + passive leak
        Jerout = (vip3 * ( ip3 ** 2. / (ip3 ** 2. + kip3 ** 2.)) * (cac ** 2. / (cac ** 2. + kcaa ** 2. )) * (kcai1 ** 4. / (kcai2 ** 4. + cac ** 4.)) + vleak) * (caer - cac) / uMmM
        
        # Phosphorylation of mitochondrial ADP via F1F0 ATPase
        deltaMuH = psi - 2.303 * RTF * DpH
        VDf1 = np.exp(3. * psi / RTF)
        Af1 = kf1 * atpm / (adpm * pim)
        Jf1 = - rhof1 * (Pac1 * Af1 - pa * VDf1 + pc2 * Af1 * VDf1) / ((1. + p1 * Af1) * VDf1B + (p2 + p3 * Af1) * VDf1)
        
        # Fumarate hydratase
        Jfh = kffh * (fum - mal/kefh)
        
        # Proton leak from cytosol to mitochondria
        Jhl = gH * deltaMuH
        
        # Isocitrate dehydrogenase
        Jidh = Vmaxidh / ( 1. + hm/kh1 + kh2/hm + (kmisoc/isoc) ** ni /((1. + adpm/kaadp) * (1. + cam/kaca)) + (kmnad/nadm)*(1. + nadhm/kinadh) + (kmisoc/isoc) ** ni *(kmnad/nadm)*(1. + nadhm/kinadh) /((1. + adpm/kaadp) * (1. + cam/kaca)))
        
        # Alpha-ketoglutarate dehydrogenase
        Jkgdh = Vmaxkgdh / (1. + (kmakg / akg) /((1. + mg/kdmg) * (1. + cam/kdca)) + (kmnadkgdh/nadm) ** nakg /((1. + mg/kdmg) * (1. + cam/kdca)))
        
        # Malate dehydrogenase
        fha = 0.351195
        fhi = 0.999346
        Jmdh = Vmdh * (mal * nadm - oaa * nadhm / Keqmdh) / ((1. + mal/kmmal) * (1. + nadm/kmnadmdh) + (1. + oaa/kmoaamdh)*(1. + nadhm/kmnadhmdh) - 1.)
        
        # Ca2+ release from mitochondria via NCX (Cortassa 2003)
        Jncx = vncx * (cam / cac) * np.exp(b * (psi - psiStar) / RTF) / ((1. + kca / cam) * (1. + kna / nac) ** n)
        
        # Oxidation of NADH by respiration
        VDres = np.exp(6. * g * psi / RTF)
        Ares = kres * (nadhm / nadm) ** 0.5
        Jo = 0.5 * rhores * (Rac1 * Ares - ra * VDres + rc2 * Ares * VDres ) / ((1. + r1 * Ares) * VDresB + (r2 + r3 * Ares) * VDres)
        
        # Hydrolysis of cytosolic ATP
        Jhyd = khyd * atpc / (katpcH + atpc)
        
        # Succinate dehydrogenase
        Jsdh = Vmaxsdh / (1. + (kmsuc/suc) * (1. + oaa/kioaasdh) * (1. + fum/kifum))
        
        # SERCA pumps
        Jserca = vserca * (cac ** 2. / (kserca ** 2. + cac ** 2.))	# uncoupled
        
        # Succinyl Coa Lyase
        Jsl = kfsl * (scoa * adpm * pim*1.e3 - suc * atpm * coa / kesl)
        
        # Ca2+ uptake to mitochondria via MCU
        VDuni = 2. * (psi - psiStar) / RTF
        trc = cac / ktrans
        mwc = trc * (1. + trc) ** 3. / ((1. + trc) ** 4. + L / (1. + cac / kact) ** ma)
        #Juni = vuni * VDuni * (mwc - cam * np.exp(-VDuni)) / (1. - np.exp(-VDuni))
        Juni=vuni * VDuni * mwc / (1. - np.exp(-VDuni))
       
        return Jaco, Jant, Jcs, Jerout, Jf1, Jfh, Jhl, Jhyd, Jidh, Jkgdh, Jmdh, Jncx, Jo, Jsdh, Jserca, Jsl, Juni;
        
def mmk(s, v):
       
        Jaco, Jant, Jcs, Jerout, Jf1, Jfh, Jhl, Jhyd, Jidh, Jkgdh, Jmdh, Jncx, Jo, Jsdh, Jserca, Jsl, Juni = fluxes(v)
              
        dvds=np.zeros((len(v),1))
        dvds[0] = (-delta * Jant + Jhyd)                                                # adpc, uncoupled -- stoichiometry serca: cf. Tran 2009
        dvds[1] = Jant - Jf1 - Jsl                                                      # adpm
        dvds[2] = Jidh - Jkgdh                                                          # akg
        dvds[3] = 0.                                                                    # asp -- not used in this work
        dvds[4] = -dvds[0]                                                              # atpc
        dvds[5] = -dvds[1]                                                              # atpm
        dvds[6] = uMmM * fc * (-Jserca + Jerout + delta * (Jncx - Juni))                # cac
        dvds[7] = uMmM * fm * (Juni - Jncx)                                             # cam
        dvds[8] = Jcs - Jaco                                                            # cit
        dvds[9] = Jsdh - Jfh                                                            # fum
        dvds[10] = Jaco - Jidh                                                          # isoc
        dvds[11] = Jfh - Jmdh                                                           # mal
        dvds[12] = -Jo + Jidh + Jkgdh + Jmdh                                            # nadhm
        dvds[13] = -dvds[12]                                                            # nadm
        dvds[14] = Jmdh - Jcs                                                           # oaa
        dvds[15] = (10.*Jo - 3.*Jf1 - Jant - Jhl - Jncx - 2. * Juni) / cmito            # psi
        dvds[16] = Jkgdh - Jsl                                                          # scoa
        dvds[17] = Jsl - Jsdh                                                           # suc
        
        return dvds                                     # (in uM/s or mM/s))

def jacMatrix(s, v):
	[adpc, adpm, akg, asp, atpc, atpm, cac, cam, cit, fum, isoc, mal, nadhm, nadm, oaa, psi, scoa, suc]=v
	
	jm = np.zeros((len(v), len(v)))
	
	jm[0] = [(alphac*atpc*delta*np.exp(((-1+f)*psi)/RTF)*(-alpham*atpm*np.exp(psi/RTF)-adpm*np.exp((f*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.),(alpham*atpm*delta*np.exp(((-1+f)*psi)/RTF)*(alphac*atpc+adpc*np.exp(psi/RTF))*vant)/((adpm+alpham*atpm)**2.*(alphac*atpc+adpc*np.exp((f*psi)/RTF))),0.,0.,(katpcH*khyd)/(atpc+katpcH)**2.+(adpc*alphac*delta*(alpham*atpm*np.exp((f*psi)/RTF)+adpm*np.exp(((-1+2*f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.),(adpm*alpham*delta*np.exp(((-1+f)*psi)/RTF)*(-alphac*atpc-adpc*np.exp(psi/RTF))*vant)/((adpm+alpham*atpm)**2.*(alphac*atpc+adpc*np.exp((f*psi)/RTF))),0.,0.,0.,0.,0.,0.,0.,0.,0.,(alphac*atpc*delta*np.exp(((-1+f)*psi)/RTF)*(-adpc*adpm*np.exp((f*psi)/RTF)-adpc*alpham*atpm*np.exp(psi/RTF)*f+adpm*alphac*atpc*(-1.+f))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.*RTF),0.,0.]
	
	jm[1] = [(alphac*atpc*np.exp((f*psi)/RTF)*(alpham*atpm+adpm*np.exp(((-1+f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2),-1000*kfsl*pim*scoa-(alphac*atpc*np.exp(((-1+f)*psi)/RTF)*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))+(np.exp(((-1+f)*psi)/RTF)*(adpm*alphac*atpc*vant-adpc*alpham*atpm*np.exp(psi/RTF)*vant))/((adpm+alpham*atpm)**2*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))-(atpm*kf1*(atpm*kf1*(-Pac1-np.exp((3.*psi)/RTF)*pc2)+adpm*np.exp((3.*psi)/RTF)*pa*pim)*rhof1*(np.exp((3.*psi)/RTF)*p3+p1*VDf1B))/(adpm*(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2)-(atpm*kf1*(Pac1+np.exp((3.*psi)/RTF)*pc2)*rhof1)/(adpm*(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)),0,0,(adpc*alphac*np.exp((f*psi)/RTF)*(-alpham*atpm-adpm*np.exp(((-1+f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2),(coa*kfsl*suc)/kesl+(adpm*alphac*atpc*np.exp(((-1+f)*psi)/RTF)*vant)/(atpm*(adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))-(adpm*np.exp(((-1+f)*psi)/RTF)*(adpm*alphac*atpc-adpc*alpham*atpm*np.exp(psi/RTF))*vant)/(atpm*(adpm+alpham*atpm)**2*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))+(kf1*(atpm*kf1*(-Pac1-np.exp((3.*psi)/RTF)*pc2)+adpm*np.exp((3.*psi)/RTF)*pa*pim)*rhof1*(np.exp((3.*psi)/RTF)*p3+p1*VDf1B))/(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2+(kf1*(Pac1+np.exp((3.*psi)/RTF)*pc2)*rhof1)/(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B),0,0,0,0,0,0,0,0,0,(1/RTF)*((adpm*alphac*atpc*np.exp(((-1+f)*psi)/RTF)*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))-(alphac*atpc*np.exp(((-1+f)*psi)/RTF)*(adpm*alphac*atpc-adpc*alpham*atpm*np.exp(psi/RTF))*f*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2)+(3.*np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)*(atpm*kf1*(-Pac1-np.exp((3.*psi)/RTF)*pc2)+adpm*np.exp((3.*psi)/RTF)*pa*pim)*rhof1)/(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2+(3.*np.exp((3.*psi)/RTF)*(atpm*kf1*pc2-adpm*pa*pim)*rhof1)/(atpm*np.exp((3.*psi)/RTF)*kf1*p3+adpm*np.exp((3.*psi)/RTF)*p2*pim+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)),-1000*adpm*kfsl*pim,(atpm*coa*kfsl)/kesl]
	
	jm[2] = [0.,-((Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm))/((adpm+kaadp)**2*(cam+kaca)*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2)),-((Vmaxkgdh*kdca*(cam+kdca)*kdmg*kmakg*(kdmg+mg))/(kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2),0.,0.,0.,0.,-((akg*Vmaxkgdh*kdca*kdmg*(kdmg+mg)*(kmakg+akg*(kmnadkgdh/nadm)**nakg))/(kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2)-(Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm))/((adpm+kaadp)*(cam+kaca)**2*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2),0.,0.,-((Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm)*ni)/(isoc*(adpm+kaadp)*(cam+kaca)*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2)),0.,-((Vmaxidh*(1+(kaadp*kaca*(kmisoc/isoc)**ni)/((adpm+kaadp)*(cam+kaca)))*kmnad)/(kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2)),-((Vmaxidh*(-1-(kaadp*kaca*(kmisoc/isoc)**ni)/((adpm+kaadp)*(cam+kaca)))*kmnad*(1.+nadhm/kinadh))/(kmnad+(kmnad*nadhm)/kinadh+nadm+(hm*nadm)/kh1+(kh2*nadm)/hm+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh))**2)-(akg**2*Vmaxkgdh*kdca*(cam+kdca)*kdmg*(kdmg+mg)*(kmnadkgdh/nadm)**nakg*nakg)/((kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2*nadm),0.,0.,0.,0.]
	
	jm[3] = [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]
	
	jm[4] = [(alphac*atpc*delta*np.exp(((-1+f)*psi)/RTF)*(alpham*atpm*np.exp(psi/RTF)+adpm*np.exp((f*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.),(alpham*atpm*delta*np.exp(((-1+f)*psi)/RTF)*(-alphac*atpc-adpc*np.exp(psi/RTF))*vant)/((adpm+alpham*atpm)**2.*(alphac*atpc+adpc*np.exp((f*psi)/RTF))),0.,0.,-((katpcH*khyd)/(atpc+katpcH)**2.)+(adpc*alphac*delta*(-alpham*atpm*np.exp((f*psi)/RTF)-adpm*np.exp(((-1+2.*f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.),(adpm*alpham*delta*np.exp(((-1+f)*psi)/RTF)*(alphac*atpc+adpc*np.exp(psi/RTF))*vant)/((adpm+alpham*atpm)**2.*(alphac*atpc+adpc*np.exp((f*psi)/RTF))),0.,0.,0.,0.,0.,0.,0.,0.,0.,(alphac*atpc*delta*np.exp(((-1+f)*psi)/RTF)*(adpc*adpm*np.exp((f*psi)/RTF)+adpm*alphac*atpc*(1.-f)+adpc*alpham*atpm*np.exp(psi/RTF)*f)*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2.*RTF),0.,0.]
	
	jm[5] = [(alphac*atpc*np.exp((f*psi)/RTF)*(-alpham*atpm-adpm*np.exp(((-1+f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2),1000*kfsl*pim*scoa+(alphac*atpc*np.exp(((-1+f)*psi)/RTF)*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))+(np.exp(((-1+f)*psi)/RTF)*(-adpm*alphac*atpc+adpc*alpham*atpm*np.exp(psi/RTF))*vant)/((adpm+alpham*atpm)**2*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))+(atpm*kf1*(-atpm*kf1*Pac1+np.exp((3.*psi)/RTF)*(-atpm*kf1*pc2+adpm*pa*pim))*rhof1*(np.exp((3.*psi)/RTF)*p3+p1*VDf1B))/(adpm*(np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2)+(atpm*kf1*(Pac1+np.exp((3.*psi)/RTF)*pc2)*rhof1)/(adpm*np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)+adpm*(atpm*kf1*p1+adpm*pim)*VDf1B),0.,0.,(adpc*alphac*np.exp((f*psi)/RTF)*(alpham*atpm+adpm*np.exp(((-1+f)*psi)/RTF))*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2),-((coa*kfsl*suc)/kesl)+(adpm*alpham*(-alphac*atpc*np.exp(((-1+f)*psi)/RTF)-adpc*np.exp((f*psi)/RTF))*vant)/((adpm+alpham*atpm)**2*(alphac*atpc+adpc*np.exp((f*psi)/RTF)))+(adpm*kf1*pim*rhof1*(np.exp((6.*psi)/RTF)*(-p3*pa-p2*pc2)-Pac1*VDf1B+np.exp((3.*psi)/RTF)*(-p2*Pac1-p1*pa*VDf1B-pc2*VDf1B)))/(np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2,0.,0.,0.,0.,0.,0.,0.,0.,0.,1/RTF*(-((adpm*alphac*atpc*np.exp(((-1+f)*psi)/RTF)*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))))+(alphac*atpc*np.exp(((-1+f)*psi)/RTF)*(adpm*alphac*atpc-adpc*alpham*atpm*np.exp(psi/RTF))*f*vant)/((adpm+alpham*atpm)*(alphac*atpc+adpc*np.exp((f*psi)/RTF))**2)+(3.*np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)*(atpm*kf1*Pac1+np.exp((3.*psi)/RTF)*(atpm*kf1*pc2-adpm*pa*pim))*rhof1)/(np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)**2+(np.exp((3.*psi)/RTF)*(-3.*atpm*kf1*pc2+3.*adpm*pa*pim)*rhof1)/(np.exp((3.*psi)/RTF)*(atpm*kf1*p3+adpm*p2*pim)+atpm*kf1*p1*VDf1B+adpm*pim*VDf1B)),1000*adpm*kfsl*pim,-((atpm*coa*kfsl)/kesl)]
	
	jm[6] = [0.,0.,0.,0.,-((cac**2.*fc*katpc*uMmM*vserca)/((atpc+katpc)**2*(cac**2.+kserca**2.))),0.,(2.*(cac-1.*coef1+cac*coef2+cam*coef3)*fc*ip3**2.*(2.*cac**7.*kcai1**4.+1.*cac**5.*kcaa**2.*kcai1**4.-1.*cac**1.*kcaa**2.*kcai1**8.)*vip3)/((cac**2.+kcaa**2.)**2*(cac**4.+kcai1**4.)**2*(ip3**2.+kip3**2.))+(-1-coef2)*fc*((cac**2.*ip3**2.*kcai1**4.*vip3)/((cac**2.+kcaa**2.)*(cac**4.+kcai1**4.)*(ip3**2.+kip3**2.))+vleak)+(2.*atpc*cac**3.*fc*uMmM*vserca)/((atpc+katpc)*(cac**2.+kserca**2.)**2)-(2.*atpc*cac**1.*fc*uMmM*vserca)/((atpc+katpc)*(cac**2.+kserca**2.))+delta*fc*uMmM*(-((cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**-n*vncx)/(cac**2*(1.+kca/cam)))-(6.*cac*(1.+cac/ktrans)**2.*(psi-psiStar)*vuni)/((1.*-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans**2*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF)-(2.*(1.+cac/ktrans)**3.*(psi-psiStar)*vuni)/((1.*-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF)+(2.*cac*(1.+cac/ktrans)**3.*((4.*(1.+cac/ktrans)**3.)/ktrans-((1.+cac/kact)**(-1-ma)*L*ma)/kact)*(psi-psiStar)*vuni)/((1.*-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)**2*RTF)),-coef3*fc*((cac**2.*ip3**2.*kcai1**4.*vip3)/((cac**2.+kcaa**2.)*(cac**4.+kcai1**4.)*(ip3**2.+kip3**2.))+vleak)+(1.*cam*delta*np.exp((b*(psi-psiStar))/RTF)*fc*(1.*cam+2.*kca)*(1.+kna/nac)**-n*uMmM*vncx)/(cac*(1.*cam+1.*kca)**2),0.,0.,0.,0.,0.,0.,0.,delta*fc*uMmM*((b*cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**-n*vncx)/(cac*(1.+kca/cam)*RTF)+(cac*(1.+cac/kact)**ma*(1.+cac/ktrans)**3.*(-2.*np.exp((4.*psi)/RTF)*RTF+np.exp((2.*psi+2.*psiStar)/RTF)*(4.*psi-4.*psiStar+2.*RTF))*vuni)/((1.*np.exp((2.*psi)/RTF)-1.*np.exp((2.*psiStar)/RTF))**2*ktrans*((1.+cac/kact)**ma*(1.+cac/ktrans)**4.+L)*RTF**2)),0.,0.]
	
	jm[7] = [0.,0.,0.,0.,0.,0.,fm*uMmM*((cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**(-n)*vncx)/(cac**2*(1.+kca/cam))+(6.*cac*(1.+cac/ktrans)**2.*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans**2*((1.+cac/ktrans)**4.+(1.+cac/kact)**(-ma)*L)*RTF)+(2.*(1.+cac/ktrans)**3.*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**(-ma)*L)*RTF)-(2.*cac*(1.+cac/ktrans)**3.*((4.*(1.+cac/ktrans)**3.)/ktrans-((1.+cac/kact)**(-1-ma)*L*ma)/kact)*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**(-ma)*L)**2*RTF)),-((cam*np.exp((b*(psi-psiStar))/RTF)*fm*(cam+2.*kca)*(1.+kna/nac)**(-n)*uMmM*vncx)/(cac*(cam+kca)**2)),0.,0.,0.,0.,0.,0.,0.,fm*uMmM*(-((b*cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**(-n)*vncx)/(cac*(1.+kca/cam)*RTF))+(cac*(1.+cac/kact)**ma*(1.+cac/ktrans)**3.*(np.exp((2.*psi+2.*psiStar)/RTF)*(-4.*psi+4.*psiStar-2.*RTF)+2.*np.exp((4.*psi)/RTF)*RTF)*vuni)/((np.exp((2.*psi)/RTF)-np.exp((2.*psiStar)/RTF))**2*ktrans*((1.+cac/kact)**ma*(1.+cac/ktrans)**4.+L)*RTF**2)),0.,0.]
	
	jm[8] = [0.,0.,0.,0.,0.,0.,0.,0.,-kfaco,0.,kfaco/keaco,0.,0.,0.,(accoa*Vmaxcs*kiaccoa*kmoaa*(accoa**2+accoa*kiaccoa+kiaccoa*ksaccoa))/(accoa**2*kmoaa+accoa*kiaccoa*(kmoaa+oaa)+kiaccoa*(kmoaa*ksaccoa+kmaccoa*oaa))**2,0.,0.,0.]
	
	jm[9] = [0.,0.,0.,0.,0.,0.,0.,0.,0.,-kffh+(Vmaxsdh*kifum*kioaasdh*kmsuc*(-kioaasdh-oaa)*suc)/(fum*kioaasdh*kmsuc+kifum*kioaasdh*kmsuc+fum*kmsuc*oaa+kifum*kmsuc*oaa+kifum*kioaasdh*suc)**2,0.,kffh/kefh,0.,0.,-((Vmaxsdh*kifum*(fum+kifum)*kioaasdh*kmsuc*suc)/(fum*kioaasdh*kmsuc+kifum*kioaasdh*kmsuc+fum*kmsuc*oaa+kifum*kmsuc*oaa+kifum*kioaasdh*suc)**2),0.,0.,(Vmaxsdh*(1.+fum/kifum)*kmsuc*(1.+oaa/kioaasdh))/((1.+fum/kifum)*kmsuc*(1.+oaa/kioaasdh)+suc)**2]
	
	jm[10] = [0,(Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm))/((adpm+kaadp)**2*(cam+kaca)*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2),0,0,0,0,0,(Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm))/((adpm+kaadp)*(cam+kaca)**2*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2),kfaco,0,-(kfaco/keaco)+(Vmaxidh*kaadp*kaca*(kmisoc/isoc)**ni*(-kinadh*kmnad-kmnad*nadhm-kinadh*nadm)*ni)/(isoc*(adpm+kaadp)*(cam+kaca)*kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2),0,(Vmaxidh*(1+(kaadp*kaca*(kmisoc/isoc)**ni)/((adpm+kaadp)*(cam+kaca)))*kmnad)/(kinadh*nadm*(1.+hm/kh1+kh2/hm+(kmnad)/nadm+(kmnad*nadhm)/(kinadh*nadm)+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh*nadm))**2),(Vmaxidh*(-1-(kaadp*kaca*(kmisoc/isoc)**ni)/((adpm+kaadp)*(cam+kaca)))*kmnad*(1.+nadhm/kinadh))/(kmnad+(kmnad*nadhm)/kinadh+nadm+(hm*nadm)/kh1+(kh2*nadm)/hm+(kaadp*kaca*(kmisoc/isoc)**ni*(kinadh*kmnad+kmnad*nadhm+kinadh*nadm))/((adpm+kaadp)*(cam+kaca)*kinadh))**2,0,0,0,0]
	
	jm[11] = [0,0,0,0,0,0,0,0,0,kffh,0,-(kffh/kefh)+((1.+nadm/kmnadmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmmal*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)-(nadm*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),(kmmal*kmnadhmdh*kmnadmdh*kmoaamdh*(Keqmdh*kmmal*kmnadmdh*mal*nadm*(kmoaamdh+oaa)+kmnadhmdh*oaa*(kmoaamdh*mal*(kmnadmdh+nadm)+kmmal*(kmnadmdh*kmoaamdh+kmoaamdh*nadm+kmnadmdh*oaa)))*Vmdh)/(Keqmdh*(kmnadhmdh*kmoaamdh*mal*(kmnadmdh+nadm)+kmmal*kmnadmdh*nadhm*(kmoaamdh+oaa)+kmmal*kmnadhmdh*(kmnadmdh*kmoaamdh+kmoaamdh*nadm+kmnadmdh*oaa))**2),(kmmal*kmnadhmdh*kmnadmdh*kmoaamdh*(kmnadhmdh*kmoaamdh*(-kmmal-mal)*nadhm*oaa+Keqmdh*kmnadmdh*mal*(-kmnadhmdh*kmoaamdh*mal+kmmal*(-kmnadhmdh*kmoaamdh-kmoaamdh*nadhm-kmnadhmdh*oaa-nadhm*oaa)))*Vmdh)/(Keqmdh*(kmnadhmdh*kmoaamdh*mal*(kmnadmdh+nadm)+kmmal*kmnadmdh*nadhm*(kmoaamdh+oaa)+kmmal*kmnadhmdh*(kmnadmdh*kmoaamdh+kmoaamdh*nadm+kmnadmdh*oaa))**2),(kmmal*kmnadhmdh*kmnadmdh*kmoaamdh*(kmnadhmdh*kmoaamdh*mal*nadhm*(kmnadmdh+nadm)+kmmal*kmnadmdh*nadhm*(kmoaamdh*nadhm+Keqmdh*mal*nadm)+kmmal*kmnadhmdh*(kmnadmdh*kmoaamdh*nadhm+Keqmdh*kmnadmdh*mal*nadm+kmoaamdh*nadhm*nadm))*Vmdh)/(Keqmdh*(kmnadhmdh*kmoaamdh*mal*(kmnadmdh+nadm)+kmmal*kmnadmdh*nadhm*(kmoaamdh+oaa)+kmmal*kmnadhmdh*(kmnadmdh*kmoaamdh+kmoaamdh*nadm+kmnadmdh*oaa))**2),0,0,0]
	
	jm[12] = [0,-((Vmaxidh*(-((kmisoc/isoc)**ni/((1.+adpm/kaadp)**2*kaadp*(1.+cam/kaca)))-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)**2*kaadp*(1.+cam/kaca)*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2),(Vmaxkgdh*kmakg)/(akg**2*(1.+cam/kdca)*(1.+mg/kdmg)*(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2),0,0,0,0,-((Vmaxkgdh*(-(kmakg/(akg*(1.+cam/kdca)**2*kdca*(1.+mg/kdmg)))-(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)**2*kdca*(1.+mg/kdmg))))/(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2)-(Vmaxidh*(-((kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca)**2*kaca))-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)**2*kaca*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2,0,0,-((Vmaxidh*(-((kmisoc*(kmisoc/isoc)**(-1+ni)*ni)/(isoc**2*(1.+adpm/kaadp)*(1.+cam/kaca)))-(kmisoc*(kmisoc/isoc)**(-1+ni)*kmnad*(1.+nadhm/kinadh)*ni)/(isoc**2*(1.+adpm/kaadp)*(1.+cam/kaca)*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2),-(((1.+nadm/kmnadmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmmal*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2))+(nadm*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),-((Vmaxidh*(kmnad/(kinadh*nadm)+((kmisoc/isoc)**ni*kmnad)/((1.+adpm/kaadp)*(1.+cam/kaca)*kinadh*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2)+(0.5*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*((0.5*np.exp((6.*g*psi)/RTF)*kres*r3)/((nadhm/nadm)**0.5*nadm)+(0.5*kres*r1*VDresB)/((nadhm/nadm)**0.5*nadm)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2-(0.5*((0.5*kres*Rac1)/((nadhm/nadm)**0.5*nadm)+(0.5*np.exp((6.*g*psi)/RTF)*kres*rc2)/((nadhm/nadm)**0.5*nadm))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)-((1.+oaa/kmoaamdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadhmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)-(oaa*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),-((Vmaxidh*(-((kmnad*(1.+nadhm/kinadh))/nadm**2)-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm**2)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2)+(Vmaxkgdh*kmnadkgdh*(kmnadkgdh/nadm)**(-1+nakg)*nakg)/((1.+cam/kdca)*(1.+mg/kdmg)*(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2*nadm**2)+(0.5*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*(-((0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*r3)/((nadhm/nadm)**0.5*nadm**2))-(0.5*kres*nadhm*r1*VDresB)/((nadhm/nadm)**0.5*nadm**2)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2-(0.5*(-((0.5*kres*nadhm*Rac1)/((nadhm/nadm)**0.5*nadm**2))-(0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*rc2)/((nadhm/nadm)**0.5*nadm**2))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)-((1.+mal/kmmal)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)+(mal*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),-(((1.+nadhm/kmnadhmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmoaamdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2))-(nadhm*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),(3.*np.exp((6.*g*psi)/RTF)*g*(r2+kres*(nadhm/nadm)**0.5*r3)*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores)/(RTF*(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2)-(0.5*rhores*(-((6.*np.exp((6.*g*psi)/RTF)*g*ra)/RTF)+(6.*np.exp((6.*g*psi)/RTF)*g*kres*(nadhm/nadm)**0.5*rc2)/RTF))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB),0,0]
	
	jm[13] = [0,(Vmaxidh*(-((kmisoc/isoc)**ni/((1.+adpm/kaadp)**2*kaadp*(1.+cam/kaca)))-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)**2*kaadp*(1.+cam/kaca)*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2,-((Vmaxkgdh*kmakg)/(akg**2*(1.+cam/kdca)*(1.+mg/kdmg)*(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2)),0,0,0,0,(Vmaxkgdh*(-(kmakg/(akg*(1.+cam/kdca)**2*kdca*(1.+mg/kdmg)))-(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)**2*kdca*(1.+mg/kdmg))))/(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2+(Vmaxidh*(-((kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca)**2*kaca))-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)**2*kaca*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2,0,0,(Vmaxidh*(-((kmisoc*(kmisoc/isoc)**(-1+ni)*ni)/(isoc**2*(1.+adpm/kaadp)*(1.+cam/kaca)))-(kmisoc*(kmisoc/isoc)**(-1+ni)*kmnad*(1.+nadhm/kinadh)*ni)/(isoc**2*(1.+adpm/kaadp)*(1.+cam/kaca)*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2,((1.+nadm/kmnadmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmmal*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)-(nadm*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),(Vmaxidh*(kmnad/(kinadh*nadm)+((kmisoc/isoc)**ni*kmnad)/((1.+adpm/kaadp)*(1.+cam/kaca)*kinadh*nadm)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2-(0.5*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*((0.5*np.exp((6.*g*psi)/RTF)*kres*r3)/((nadhm/nadm)**0.5*nadm)+(0.5*kres*r1*VDresB)/((nadhm/nadm)**0.5*nadm)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2+(0.5*((0.5*kres*Rac1)/((nadhm/nadm)**0.5*nadm)+(0.5*np.exp((6.*g*psi)/RTF)*kres*rc2)/((nadhm/nadm)**0.5*nadm))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)+((1.+oaa/kmoaamdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadhmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)+(oaa*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),(Vmaxidh*(-((kmnad*(1.+nadhm/kinadh))/nadm**2)-((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm**2)))/(1.+hm/kh1+kh2/hm+(kmisoc/isoc)**ni/((1.+adpm/kaadp)*(1.+cam/kaca))+(kmnad*(1.+nadhm/kinadh))/nadm+((kmisoc/isoc)**ni*kmnad*(1.+nadhm/kinadh))/((1.+adpm/kaadp)*(1.+cam/kaca)*nadm))**2-(Vmaxkgdh*kmnadkgdh*(kmnadkgdh/nadm)**(-1+nakg)*nakg)/((1.+cam/kdca)*(1.+mg/kdmg)*(1.+kmakg/(akg*(1.+cam/kdca)*(1.+mg/kdmg))+(kmnadkgdh/nadm)**nakg/((1.+cam/kdca)*(1.+mg/kdmg)))**2*nadm**2)-(0.5*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*(-((0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*r3)/((nadhm/nadm)**0.5*nadm**2))-(0.5*kres*nadhm*r1*VDresB)/((nadhm/nadm)**0.5*nadm**2)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2+(0.5*(-((0.5*kres*nadhm*Rac1)/((nadhm/nadm)**0.5*nadm**2))-(0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*rc2)/((nadhm/nadm)**0.5*nadm**2))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)+((1.+mal/kmmal)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)-(mal*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),((1.+nadhm/kmnadhmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmoaamdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)+(nadhm*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),-((3.*np.exp((6.*g*psi)/RTF)*g*(r2+kres*(nadhm/nadm)**0.5*r3)*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores)/(RTF*(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2))+(0.5*rhores*(-((6.*np.exp((6.*g*psi)/RTF)*g*ra)/RTF)+(6.*np.exp((6.*g*psi)/RTF)*g*kres*(nadhm/nadm)**0.5*rc2)/RTF))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB),0,0]
	
	jm[14] = [0,0,0,0,0,0,0,0,0,0,0,-(((1.+nadm/kmnadmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmmal*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2))+(nadm*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),-(((1.+oaa/kmoaamdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadhmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2))-(oaa*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),-(((1.+mal/kmmal)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmnadmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2))+(mal*Vmdh)/(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh)),(accoa*Vmaxcs*(-((accoa*(1.+accoa/kiaccoa)*kmoaa)/oaa**2)-(kmoaa*ksaccoa)/oaa**2))/(accoa+kmaccoa+(accoa*(1.+accoa/kiaccoa)*kmoaa)/oaa+(kmoaa*ksaccoa)/oaa)**2-((1.+nadhm/kmnadhmdh)*(mal*nadm-(nadhm*oaa)/Keqmdh)*Vmdh)/(kmoaamdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))**2)-(nadhm*Vmdh)/(Keqmdh*(-1.+(1.+mal/kmmal)*(1.+nadm/kmnadmdh)+(1.+nadhm/kmnadhmdh)*(1.+oaa/kmoaamdh))),0,0,0]
	
	jm[15] = [(-((alphac*atpc*np.exp(-((f*psi)/RTF))*(1.-(adpm*alphac*atpc*np.exp(-(psi/RTF)))/(adpc*alpham*atpm))*vant)/(adpc**2*(1.+adpm/(alpham*atpm))*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)**2))-(adpm*alphac*atpc*np.exp(-(psi/RTF))*vant)/(adpc**2*alpham*(1.+adpm/(alpham*atpm))*atpm*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)))/cmito,1/cmito*((alphac*atpc*np.exp(-(psi/RTF))*vant)/(adpc*alpham*(1.+adpm/(alpham*atpm))*atpm*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc))+((1.-(adpm*alphac*atpc*np.exp(-(psi/RTF)))/(adpc*alpham*atpm))*vant)/(alpham*(1.+adpm/(alpham*atpm))**2*atpm*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc))+(3.*(-((atpm*kf1*Pac1)/(adpm**2*pim))-(atpm*np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm**2*pim))*rhof1)/(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)-(3.*(-np.exp(((3.*psi)/RTF))*pa+(atpm*kf1*Pac1)/(adpm*pim)+(atpm*np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm*pim))*rhof1*(-((atpm*np.exp((3.*psi)/RTF)*kf1*p3)/(adpm**2*pim))-(atpm*kf1*p1*VDf1B)/(adpm**2*pim)))/(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)**2),0,0,((alphac*np.exp(-((f*psi)/RTF))*(1.-(adpm*alphac*atpc*np.exp(-(psi/RTF)))/(adpc*alpham*atpm))*vant)/(adpc*(1.+adpm/(alpham*atpm))*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)**2)+(adpm*alphac*np.exp(-(psi/RTF))*vant)/(adpc*alpham*(1.+adpm/(alpham*atpm))*atpm*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)))/cmito,1/cmito*(-((adpm*alphac*atpc*np.exp(-(psi/RTF))*vant)/(adpc*alpham*(1.+adpm/(alpham*atpm))*atpm**2*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)))-(adpm*(1.-(adpm*alphac*atpc*np.exp(-(psi/RTF)))/(adpc*alpham*atpm))*vant)/(alpham*(1.+adpm/(alpham*atpm))**2*atpm**2*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc))+(3.*((kf1*Pac1)/(adpm*pim)+(np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm*pim))*rhof1)/(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)-(3.*(-np.exp(((3.*psi)/RTF))*pa+(atpm*kf1*Pac1)/(adpm*pim)+(atpm*np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm*pim))*rhof1*((np.exp((3.*psi)/RTF)*kf1*p3)/(adpm*pim)+(kf1*p1*VDf1B)/(adpm*pim)))/(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)**2),1/cmito*((cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**-n*vncx)/(cac**2*(1.+kca/cam))-(12.*cac*(1.+cac/ktrans)**2.*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans**2*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF)-(4.*(1.+cac/ktrans)**3.*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF)+(4.*cac*(1.+cac/ktrans)**3.*((4.*(1.+cac/ktrans)**3.)/ktrans-((1.+cac/kact)**(-1-ma)*L*ma)/kact)*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)**2*RTF)),(-((np.exp((b*(psi-psiStar))/RTF)*kca*(1.+kna/nac)**-n*vncx)/(cac*cam*(1.+kca/cam)**2))-(np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**-n*vncx)/(cac*(1.+kca/cam)))/cmito,0,0,0,0,(-((5.*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*((0.5*np.exp((6.*g*psi)/RTF)*kres*r3)/((nadhm/nadm)**0.5*nadm)+(0.5*kres*r1*VDresB)/((nadhm/nadm)**0.5*nadm)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2)+(5.*((0.5*kres*Rac1)/((nadhm/nadm)**0.5*nadm)+(0.5*np.exp((6.*g*psi)/RTF)*kres*rc2)/((nadhm/nadm)**0.5*nadm))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB))/cmito,(-((5.*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores*(-((0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*r3)/((nadhm/nadm)**0.5*nadm**2))-(0.5*kres*nadhm*r1*VDresB)/((nadhm/nadm)**0.5*nadm**2)))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2)+(5.*(-((0.5*kres*nadhm*Rac1)/((nadhm/nadm)**0.5*nadm**2))-(0.5*np.exp((6.*g*psi)/RTF)*kres*nadhm*rc2)/((nadhm/nadm)**0.5*nadm**2))*rhores)/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB))/cmito,0,1/cmito*(-gH-(adpm*alphac*atpc*np.exp(-(psi/RTF))*vant)/(adpc*alpham*(1.+adpm/(alpham*atpm))*atpm*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)*RTF)-(alphac*atpc*np.exp(-((f*psi)/RTF))*(1.-(adpm*alphac*atpc*np.exp(-(psi/RTF)))/(adpc*alpham*atpm))*f*vant)/(adpc*(1.+adpm/(alpham*atpm))*(1.+(alphac*atpc*np.exp(-((f*psi)/RTF)))/adpc)**2*RTF)-(9.*np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))*(-np.exp(((3.*psi)/RTF))*pa+(atpm*kf1*Pac1)/(adpm*pim)+(atpm*np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm*pim))*rhof1)/(RTF*(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)**2)+(3.*rhof1*(-((3.*np.exp((3.*psi)/RTF)*pa)/RTF)+(3.*atpm*np.exp((3.*psi)/RTF)*kf1*pc2)/(adpm*pim*RTF)))/(np.exp((3.*psi)/RTF)*(p2+(atpm*kf1*p3)/(adpm*pim))+(1.+(atpm*kf1*p1)/(adpm*pim))*VDf1B)-(30.*np.exp((6.*g*psi)/RTF)*g*(r2+kres*(nadhm/nadm)**0.5*r3)*(-np.exp(((6.*g*psi)/RTF))*ra+kres*(nadhm/nadm)**0.5*Rac1+np.exp((6.*g*psi)/RTF)*kres*(nadhm/nadm)**0.5*rc2)*rhores)/(RTF*(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)**2)+(5.*rhores*(-((6.*np.exp((6.*g*psi)/RTF)*g*ra)/RTF)+(6.*np.exp((6.*g*psi)/RTF)*g*kres*(nadhm/nadm)**0.5*rc2)/RTF))/(np.exp((6.*g*psi)/RTF)*(r2+kres*(nadhm/nadm)**0.5*r3)+(1.+kres*(nadhm/nadm)**0.5*r1)*VDresB)-(b*cam*np.exp((b*(psi-psiStar))/RTF)*(1.+kna/nac)**-n*vncx)/(cac*(1.+kca/cam)*RTF)+(8.*cac*np.exp(-((2.*(psi-psiStar))/RTF))*(1.+cac/ktrans)**3.*(psi-psiStar)*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))**2*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF**2)-(4.*cac*(1.+cac/ktrans)**3.*vuni)/((1.-np.exp(-((2.*(psi-psiStar))/RTF)))*ktrans*((1.+cac/ktrans)**4.+(1.+cac/kact)**-ma*L)*RTF)),0,0]
	
	jm[16] = [0,-1000*kfsl*pim*scoa,(Vmaxkgdh*kdca*(cam+kdca)*kdmg*kmakg*(kdmg+mg))/(kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2,0,0,(coa*kfsl*suc)/kesl,0,(akg*Vmaxkgdh*kdca*kdmg*(kdmg+mg)*(kmakg+akg*(kmnadkgdh/nadm)**nakg))/(kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2,0,0,0,0,0,(akg**2*Vmaxkgdh*kdca*(cam+kdca)*kdmg*(kdmg+mg)*(kmnadkgdh/nadm)**nakg*nakg)/((kdca*kdmg*kmakg+akg*(cam*kdmg+kdca*kdmg+cam*mg+kdca*mg+kdca*kdmg*(kmnadkgdh/nadm)**nakg))**2*nadm),0,0,-1000*adpm*kfsl*pim,(atpm*coa*kfsl)/kesl]
	
	jm[17] = [0,1000*kfsl*pim*scoa,0,0,0,-((coa*kfsl*suc)/kesl),0,0,0,(Vmaxsdh*kmsuc*(1.+oaa/kioaasdh))/(kifum*(1.+((1.+fum/kifum)*kmsuc*(1.+oaa/kioaasdh))/suc)**2*suc),0,0,0,0,(Vmaxsdh*kifum*(fum+kifum)*kioaasdh*kmsuc*suc)/(fum*kioaasdh*kmsuc+kifum*kioaasdh*kmsuc+fum*kmsuc*oaa+kifum*kmsuc*oaa+kifum*kioaasdh*suc)**2,0,1000*adpm*kfsl*pim,-((atpm*coa*kfsl)/kesl)-(Vmaxsdh*kifum*(fum+kifum)*kioaasdh*kmsuc*(kioaasdh+oaa))/(fum*kioaasdh*kmsuc+kifum*kioaasdh*kmsuc+fum*kmsuc*oaa+kifum*kmsuc*oaa+kifum*kioaasdh*suc)**2]
	
	return jm

def deltaG(v):
        # Concentrations appearing in \Delta G are renormalised with respect to the same volume of reference as the corresponding flux.
        
        # For convenience of notation
        [adpc, adpm, akg, asp, atpc, atpm, cac, cam, cit, fum, isoc, mal, nadhm, nadm, oaa, psi, scoa, suc]=v
        caer = coef1 - cac * coef2 - cam * coef3
        
        DGserca = 0.5*DGhydc0 + RT * np.log(adpc**0.5 * (1.e-3 * pic)**0.5 * caer / (atpc**0.5 * cac))
        DGerout = RT * np.log(cac / caer)
        DGuni = RT * np.log(cam / cac) - 2. * F * psi
        DGncx = RT * np.log((cac * nam ** 3.) / (cam * nac ** 3.)) - F * psi
        atpc4m = 0.05 * atpc
        atpm4m = 0.05 * atpm
        adpc3m = 0.45 * adpc
        adpm3m = 0.45 * 0.8 * adpm
        DGant = RT * np.log(atpc4m * adpm3m / (atpm4m * adpc3m)) - F * psi
        DGf1 = - DGhydm0 + RT * np.log(hm ** 3. * atpm / (adpm * pim * hc ** 3.))- 3. * F * psi
        DGhl = RT * np.log(hm / hc) - F * psi
        DGO = DGO0 + RT * np.log(hc ** 10. * nadm / (hm ** 10. * nadhm * o2 ** 0.5)) + 10. * F * psi
        # Real vectorial equation: nadh + 11 Hm + 0.5 O2 = Nad + 10 Hc + H2O
        DGhyd = DGhydc0 + RT * np.log(1.e-3 * adpc * pic / atpc)
        DGcs = DGcs0 + RT * np.log(cit * coa / (oaa * accoa))
        DGaco = DGaco0 + RT * np.log(isoc / cit)
        DGidh = DGidh0 + RT * np.log(1.e-3 * akg * co2 * nadhm / (isoc * nadm))
        DGkgdh = DGkgdh0 + RT * np.log(scoa * nadhm * co2 / (akg * nadm * coa))
        DGsl = DGsl0 + RT * np.log(suc * coa * atpm / (scoa * adpm * pim * 1.e3))
        DGsdh = DGsdh0 + RT * np.log(fum * coqh2 / (suc * coq))
        DGfh = DGfh0 + RT * np.log(mal / fum)
        DGmdh = DGmdh0 + RT * np.log(oaa * nadhm / (nadm * mal))
        
        return [DGaco, DGant, DGcs, DGerout, DGf1, DGfh, DGhl, DGhyd, DGidh, DGkgdh, DGmdh, DGncx, DGO, DGsdh, DGserca, DGsl, DGuni]


# KINETIC PARAMETERS
# ------------------
# Conversions (nmol/(ml*min) = uM/min)
uMmM = 1000.            # (uM/mM)

# Conservation relations
amtot = 15.                     # = atpm + adpm (mM)
atot = 3.                       # = atpc + adpc (mM)
ckint = 1.                      # = cit + isoc + akg + scoa + suc + fum + mal + oaa (mM) -- Cortassa 2003
nadtot = 0.8                    # = nadm + nadhm (mM)
ctot = 1500.                    # = cac/fc + alpha*caer/fe + delta*cam/fm (uM) -- Wacquier 2016

# Cell parameters
cmito = 1.812e-3        # mitochondrial membrane capacitance (mM/mV)
fc = 0.01               # fraction of free Ca2+ in cytosol -- MK 1998a, Fall-Keizer 2001
fe = 0.01               # fraction of free Ca2+ in ER -- Fall-Keizer 2001
fm = 0.0003             # fraction of free Ca2+ in mitochondria -- MK 1997
alpha = 0.10            # volumic ratio between ER and cytosol (Ve / Vc) -- Wacquier 2016
delta = 0.15            # volumic ratio between mito and cytosol (Vm / Vc) -- Siess et al. 1976, Lund & Wiggins 1987
coef1 = fe * ctot / alpha
coef2 = fe / (alpha * fc)
coef3 = delta * fe / (alpha * fm)

# Other fixed concentrations and cell parameters
co2 = 21.4                      # total CO2 concentration (mM) -- Beard 2007
coa = 0.02                      # concentration of coenzyme A (mM) -- Cortassa 2003
coq = 0.97                      # concentration of coenzyme q10 oxidized (mM) -- Beard 2007
coqh2 = 0.38                    # concentration of coenzyme q10 reduced (mM) -- Beard 2007
DpH = -0.8                      # = pHc - pHm -- Casey 2010
F = 96.485                      # Faraday constant in kC / mol
o2 = 2.6e-5                     # O2 concentration (M) -- Beard 2005
pic = 1.                        # inorganic phosphate concentration in cytosol (mM) -- Bevington et al. 1986
RT = 2577.34                    # = R*T, R=8.314 J/(mol*K), T=310 K
RTF = 26.7123                   # = R*T/F (1/mV), R=8.314 J/(mol*K), T=310 K, F=96485 C/mol -- MK 1997

# --- LEAK and IP3Rs ---#
vip3 = 30.                      # 15. max release rate of Ca2+ through IP3R (1/s)
vleak = 0.15                    # 0.15 rate of Ca2+ release by leakage from ER (1/s)
kcai1 = 1.4                     # Inhibition constant 1 of IP3R for Ca2+ (uM) -- Moeien thesis Table 3.1 : 1.3)
kcai2 = kcai1                   # Inhibition constant 1 of IP3R for Ca2+ (uM) -- Moeien thesis Table 3.1 : 1.3)
kcaa = 0.70                     # Activation constant of IP3R for Ca2+ (uM) -- Moien 2017 : 0.9
kip3 = 1.00                     # Activation constant of IP3R for IP3 (uM) -- Wacquier 2016

# --- SERCA ---#
#vserca = 0.12                  # max SERCA rate (mM/s)
kserca = 0.35                   # Km of SERCA pumps for Ca2+ (uM) -- Wacquier 2016, Lytton 1992
katpc = 0.05                    # Km of Serca for ATPc (mM) -- Lytton 1992, Scofano 1979: 0.05 (mM) (Moien 2017: 0.06)

# --- MCU ---#
vuni = 0.300                    # max uniporter rate at psi=psiStar (mM/s)
psiStar = 91.                   # psi offset for Ca2+ transport (mV) -- MK 1997 Table 5
L = 110.                        # Keq for uniporter conformations -- MK 1998a Table 1
ma = 2.8                        # uniporter activation cooperativity -- MK 1997 Table 5
ktrans = 19.                    # Kd for uniporter translocated Ca2+ (uM) -- MK 1998a Table 1
kact = 0.38                     # Kd for uniporter activating Ca2+ (uM) -- MK 1997 Table 5
f = 0.5                         # fraction effective psi -- MK 1997 Table 5, Cortassa 2003

# --- NCX ---#
vncx = 2.e-3                    # max NCX rate (mM/s)
n = 3.                          # nunmber of Na+ binding to NCX (electroneutral/electrogenic: n=2/3) -- MK 1997 Table 5
nac = 10.                       # cytosolic Na+ concentration (mM) -- Cortassa 2003
nam = 5.                        # mitochondrial Na+ concentration (mM) -- Donoso et al. 1992
kca = 0.375                     # Km (Ca2+) for NCX (uM) -- Bertram 2006, Cortassa 2003
b = 0.5                         # NCX dependence on psi (electroneutral/electrogenic: b=0/0.5) -- MK 1997 Table 5
kna = 9.4                       # Km (Na+) for NCX (mM) -- MK 1997 Table 5

# --- ATP hydrolysis ---#
khyd = 9.e-2                    # Basal hydrolysis rate of ATPc (1/s)
katpcH = 1.00                   # Michaelis-Menten constant for ATP hydrolysis (mM) -- Wacquier 2016

# --- ATPase ---#
rhof1 = 0.23                    # concentration of ATPase pumps (mM)
psiB = 50.                      # total phase boundary potentials (mV) -- MK 1997 Tables 2 and 3
p1 = 1.346e-8                   # MK 1997
p2 = 7.739e-7                   # MK 1997
p3 = 6.65e-15                   # MK 1997
pa = 1.656e-5                   # (1/s) -- MK 1997
pb = 3.373e-7                   # (1/s) -- MK 1997
pc1 = 9.651e-14                 # (1/s) -- MK 1997
pc2 = 4.845e-19                 # (1/s) -- MK 1997
pim = 0.020                     # inorganic phosphate concentration in mitochondrial matrix (M) -- MK 1997
kf1 = 1.71e6                    # Equilibrium constant of ATP hydrolysis -- Cortassa 2003, 2006, 2011

VDf1B = np.exp(3. * psiB / RTF)
Pa1 = pa * 10 ** (3.*DpH)
Pac1 = (Pa1 + pc1 * VDf1B) 

# --- Respiration ---#
rhores = 1.00                  # respiration-driven H+ pump concentration (mM)
r1 = 2.077e-18                  # MK 1997
r2 = 1.728e-9                   # MK 1997
r3 = 1.059e-26                  # MK 1997
ra = 6.394e-10                  # (1/s) -- MK 1997
rb = 1.762e-13                  # (1/s) -- MK 1997
rc1 = 2.656e-19                 # (1/s) -- MK 1997
rc2 = 8.632e-27                 # (1/s) -- MK 1997
g = 0.85                        # fitting factor for voltage -- MK 1997 Table 2
kres = 1.35e18                  # MK 1997 Table 2

VDresB = np.exp(6. * psiB / RTF)
Ra1 = ra * 10 ** (6.*DpH)
Rac1 = (Ra1 + rc1 * VDresB)

# --- Antiporter ---#
vant = 4.                       # (mM/s)
alphac = 0.11111                # = 0.05 / 0.45 -- Cortassa 2003, MK 1997
alpham = 0.1388889              # = 0.05 / (0.45 * 0.8) -- MK 1997

# --- Proton leak ---#
gH = 1.e-5                      # (mM/(mV * s)) -- Cortassa 2003
hc = 6.31e-5                    # cytosolic proton concentration (mM) -- Casey 2010
hm = 1.e-5                      # mitochondrial proton concentration (mM) -- Casey 2010

# --- TCA cycle --- #
# Citrate synthase
Vmaxcs = 104.                   # Vmax of citrate synthase (mM/s)
kmaccoa = 1.2614e-2             # Km of CS for AcCoa (mM) -- Cortassa 2003, Dudycha 2000: 1.26e-2 mM; BRENDA: 5-10 uM, Shepherd-Garland 1969: 16 uM
kiaccoa = 3.7068e-2             # Ki of CS for AcCoa (mM) -- Cortassa 2003
ksaccoa = 8.0749e-2             # other Km of CS for AcCoA (mM) -- Dudycha 2000
kmoaa = 5.e-3                   # Km of CS for OAA (mM) -- Cortassa 2003, Dudycha 2000: 6.2981e-4, Shepherd-Garland: 0.002, Kurz...Srere: 0.0059 mM, Matsuoka & Srere 1973+Berndt 2015 = 0.0050 mM

# 2. Aconitase
keaco = 0.067                   # equilibrium constant of aconitase -- equilibrator pH=8, I=0.12M, pMg=3.4, Berndt 2015, Garret & Grisham
kfaco = 12.5                    # forward rate constant of aconitase (1/s) -- Cortassa 2003

# 3. Isocitrate dehydrogenase
Vmaxidh = 1.7767                # Vmax of isocitrate dehydrogenase (mM/s) -- Cortassa 2003 (push conditions)
kmisoc = 1.52                   # Km of IDH for isocitrate (mM) -- Cortassa 2003
kmnad = 0.923                   # Km of IDH for NAD (mM) -- Cortassa 2003
kinadh = 0.19                   # Inhibition constant of IDH for NADHm (mM) -- Cortassa 2003
kaadp = 6.2e-2                  # Activation constant of IDH for ADPm (mM) -- Cortassa 2003
kaca = 1.41                     # Activation constant of IDH for mitochondrial Ca2+ (uM) -- Cortassa 2003
kh1 = 8.1e-5                    # Ionization constant of IDH (mM) -- Dudycha 2000, Cortassa 2003
kh2 = 5.98e-5                   # Ionisation constant of IDH (mM) -- Dudycha 2000, Cortassa 2003
ni = 2.                         # Cooperativity of Isoc in IDH -- Cortassa 2011: 2. (lacking in Cortassa 2003)

# 4. Alpha-ketoglutarate dehydrogenase
Vmaxkgdh = 2.5                  # Vmax of aKG dehydrogenase (mM/s) -- Cortassa 2003
kmakg = 1.94                    # Km of KGDH for aKG (mM) -- Cortassa 2003
kmnadkgdh = 0.0387              # Km of KGDH for NAD (mM)
nakg = 1.2                      # Hill coefficient of KGDH for aKG -- Cortassa 2003
kdca = 1.27                     # activation constant of KGDH for mitochondrial Ca2+ (uM) -- Cortassa 2003
kdmg = 0.0308                   # activation constant of KGDH for Mg2+ (mM) -- Cortassa 2003
mg = 0.4                        # mitochondrial Mg2+ concentration (mM) -- Cortassa 2003

# 5. Succinyl-CoA Lyase (Succinyl-CoA Synthetase)
kesl = 0.724                    # equilibrium constant of succinyl coa lyase -- equilibrator pH=8, I=0.12M, pMg=3.4 (reac with ADP/ATP: 0.724, with GDP/GTP: 2.152)
kfsl = 0.127                    # forward rate constant of succinyl coa lyase (1/s) -- Cortassa 2003 (0.127)

# 6. Succinate dehydrogenase
Vmaxsdh = 0.5                   # Vmax of succinate dehydrogenase (mM/s) -- Cortassa 2003
kmsuc = 3.0e-2                  # Km of SDH for succinate (mM) -- Cortassa 2003
kioaasdh = 0.15                 # Inhibition constant of SDH for OAA (mM) -- Cortassa 2003
kifum = 1.3                     # Inhibition constant of SDH for fumarate (mM) -- Cortassa 2003

# 7. Fumarate hydratase (fumarase)
kefh = 3.942                    # equilibrium constant of fumarate hydratase -- equilibrator pH=8, I=0.12M, pMg=3.4
kffh = 8.3                      # forward rate constant of fumarate hydratase (1/s) -- Cortassa 2003 (0.83)

# 8. Malate dehydrogenase
Vmdh = 128.             # mM/s
Keqmdh = 2.756e-5       # equilibrator pH=8, I=0.12M, pMg=3.4
kmmal = 0.145           # Km of MDH for malate (mM) -- Berndt 2015
kmnadmdh = 0.06         # Km of MDH for NAD (mM) -- Berndt 2015 (0.06)
kmoaamdh = 0.017        # Inhibition constant of MDH for OAA (mM) -- Berndt 2015
kmnadhmdh = 0.044       # Km of MDH for NADH (mM) -- Berndt 2015

# ------------------------------------------------------------------------
# Standard Gibbs energies of reactions at ionic strength=0.12 M (Robinson 2006), pMg=3.4, pHm=8.0 and pHc=7.2
DGhydc0 = -28300.
DGhydm0 = -32200. 
DGO0 = -225300.
DGcs0 = -41200.
DGaco0 = 6700.
DGidh0 = 5100.
DGkgdh0 = -27600.
DGsl0 = 800.                    # with ADP/ATP: 800 J/mol, with GDP/GTP: -1900 J/mol
DGsdh0 = -24200.
DGfh0 = -3400.
DGmdh0 = 24200.
DGtca0 = -59600.       # = DGcs0 + DGaco0 + DGidh0 + DGkgdh0 + DGsl0 + DGsdh0 + DGfh0 + DGmdh0

# ------------------------------------------------------------------------
# Initial conditions and some numerical parameters
first_chunk = 1                 # initialization of some parameter to plot data chunks before final save
ic = 'no'                       # 'no' : initial conditions specified explicitly; 'yes': IC=last concentrations from previous iteration
ss = 'no'                       # 'no' : steady-state not reached
regime = 'ss'                   # by default, regime defined as steady-state if no oscillations detected
tsaved = 1000.                  # s -- total period of time saved in output file
tdisc = 1000.                   # s -- period of time discarded at the beginning of the oscillations (to avoid transients)
delta_total = 0.                # s -- delta_total is initially set to 0.; increased automatically in some cases (see below) by tsaved
res_app=[]                      # initialize res_app, even if not used
dt = 1.e-2                      # time step for saving data

while ss == 'no':               # Loop that reruns the integration until the system reaches a steady-state or stable oscillations
        # Initial conditions
        if ic == 'yes':
                # Internal IC
                t0 = tf
                [adpc0, adpm0, akg0, asp0, atpc0, atpm0, cac0, cam0, cit0, fum0, isoc0, mal0, nadhm0, nadm0, oaa0, psi0, scoa0, suc0] = yf
                
                # External IC
                # t0 = np.load(filename, allow_pickle=True).item().t[-1]
                # [adpm0, atpm0, psi0, nadhm0, cit0, isoc0, akg0, scoa0, suc0, fum0, mal0, oaa0, asp0, cam0] = np.load('test.npy', allow_pickle=True).item().y[:,-1]

        elif ic == 'no':
                t0 = 0.
                adpc0 = atot*0.20               # mM
                adpm0 = amtot*0.50              # mM
                akg0 = ckint*0.01               # mM
                asp0 = 0.                       # mM
                atpc0 = atot - adpc0            # mM
                atpm0 = amtot - adpm0           # mM
                cac0 = 0.20                     # uM
                cam0 = 0.10                     # uM
                fum0 = ckint*0.01               # mM
                isoc0 = ckint*0.01              # mM
                mal0 = ckint*0.01               # mM
                nadhm0 = 0.125*nadtot           # mM
                nadm0 = nadtot - nadhm0         # mM
                oaa0 = ckint*1.e-3              # mM
                psi0 = 160.                     # mV
                scoa0 = ckint*0.01              # mM
                suc0 = ckint*0.01               # mM
                cit0 = ckint - isoc0 - akg0 - scoa0 - suc0 - fum0 - mal0 - oaa0 # mM
                
        else:
                print("Error")
        
        y0 = [adpc0, adpm0, akg0, asp0, atpc0, atpm0, cac0, cam0, cit0, fum0, isoc0, mal0, nadhm0, nadm0, oaa0, psi0, scoa0, suc0]
        #print(fluxes(y0))

        if ic == 'no':
            total = tsaved + tdisc          # s
            tchunk = total                  # s -- tsaved must be (a multiple of tchunk AND tchunk <= tsaved AND tchunk >=tdisc) OR = total
            tf = t0 + tchunk
            NTdisc = int(round((tdisc-t0)/dt))
            NTc = 1 + int(round(tf-t0)/dt)
            prt_interval = np.linspace(t0, tf, NTc, endpoint = True)
        elif ic == 'yes':
            total = t0 + tsaved + delta_total
            tchunk = total - t0             # # s -- same as above OR = total - t0
            tf = t0 + tchunk
            NTdisc = 0
            NTc = 1 + int(round(tf-t0)/dt)
            prt_interval = np.linspace(t0, tf, NTc, endpoint = True)
        else :
            print("Error")

        while tf < total + dt:
            #print("Integrating...%.0f/%.0f" % (tf, total))
            if first_chunk == 1:
                res_tmp = solve_ivp(mmk, [t0, tf], y0, t_eval = prt_interval, method='BDF', vectorized=True, atol=1.e-20, rtol=1.e-12, jac=jacMatrix, max_step=1.e-2)
                t0, tf, y0 = tf, tf + tchunk, res_tmp.y[:,-1]
                first_chunk = 0
            else:
                res_app = solve_ivp(mmk, [t0, tf], y0, t_eval = prt_interval, method='BDF', vectorized=True, atol=1.e-20, rtol=1.e-12, jac=jacMatrix, max_step=1.e-2)
                res_tmp.t, res_tmp.y = np.append(res_tmp.t, res_app.t), np.append(res_tmp.y, res_app.y, axis = 1)
                t0, tf, y0 = tf, tf + tchunk, res_app.y[:,-1]
            NTc = int(round((tf-t0)/dt))
            prt_interval = np.linspace(t0+dt, tf, NTc, endpoint=True)
            '''
            # Prepares figures for plotting 
            fig, [ax0, ax1, ax2, ax3, ax4, ax5] = plt.subplots(6,1,sharex=True)
            ax0.set_ylabel("ATPc")
            ax0b = ax0.twinx()
            ax0b.set_ylabel("ATPc:ADPc")
            ax1.set_ylabel("ATPm")
            ax1b = ax1.twinx()
            ax1b.set_ylabel("ATPm:ADPm")
            ax2.set_ylabel("$\Delta \Psi_m$")
            ax3.set_ylabel("NADHm")
            ax3b = ax3.twinx()
            ax3b.set_ylabel("NADHm:NADm")
            #ax4.set_ylabel("Jidh+Jkgdh+Jmdh")
            ax4.set_ylabel("Jo")
            ax4b = ax4.twinx()
            ax4b.set_ylabel("Jf1, Jant")
            ax5.set_ylabel("Cac")
            
            # Plotting chunks
            if res_tmp.t[NTdisc] != res_tmp.t[-1]:
                l0, l0b = ax0.plot(res_tmp.t[NTdisc:], res_tmp.y[4, NTdisc:], '-b'), ax0b.plot(res_tmp.t[NTdisc:], res_tmp.y[4, NTdisc:]/res_tmp.y[0, NTdisc:], '-y')
                l1, l1b = ax1.plot(res_tmp.t[NTdisc:], res_tmp.y[5, NTdisc:], '-b'), ax1b.plot(res_tmp.t[NTdisc:], res_tmp.y[5, NTdisc:]/res_tmp.y[1, NTdisc:], '-y')
                l2 = ax2.plot(res_tmp.t[NTdisc:], res_tmp.y[15, NTdisc:], '-b')
                l3, l3b = ax3.plot(res_tmp.t[NTdisc:], res_tmp.y[12, NTdisc:], '-b'), ax3b.plot(res_tmp.t[NTdisc:], res_tmp.y[12, NTdisc:]/res_tmp.y[13, NTdisc:], "-y")
                tmp_flux=fluxes(res_tmp.y)
                #axs[4].plot(res_tmp.t, tmp_flux[8]+tmp_flux[9]+tmp_flux[10])
                l4, l4b, l4t = ax4.plot(res_tmp.t[NTdisc:], tmp_flux[12][NTdisc:], '-b'), ax4b.plot(res_tmp.t[NTdisc:], tmp_flux[4][NTdisc:], "-y"), ax4b.plot(res_tmp.t[NTdisc:], tmp_flux[1][NTdisc:], "-r")
                l5 = ax5.plot(res_tmp.t[NTdisc:], res_tmp.y[6, NTdisc:], '-b')
                ax5.set_xlim(res_tmp.t[NTdisc], res_tmp.t[-1])
                fig.savefig("{}.png".format(filename[:-4]))
                l0.pop(0).remove(), l0b.pop(0).remove()
                l1.pop(0).remove(), l1b.pop(0).remove()
                l2.pop(0).remove(), l3.pop(0).remove(), l3b.pop(0).remove()
                l4.pop(0).remove(), l4b.pop(0).remove(), l4t.pop(0).remove(), l5.pop(0).remove()
                #print(res_tmp.y[:, -1])
            else:
                print("Identical low and high x-axis limits.")
        fig.clf()
        plt.close('all')
        '''
        res = res_tmp
        res.t, res.y = res_tmp.t[NTdisc:], res_tmp.y[:, NTdisc:]
        
        tf = res.t[-1]
        yf = res.y[:,-1]
        dyf = mmk(tf, yf)
        dyf = np.sort(np.transpose(np.abs(dyf)))
        dyf = dyf[0][::-1]
        #print(dyf)
        
        ss = 'yes'          # unless determined otherwise in the following, steady-state or stable oscillations a priori reached
        
        if dyf[0] > 1.e-10:             # non-vanishing derivatives mean either oscillations or ss not reached
        # Check if oscillations are present in Ca2+ time series
                indices, _ = find_peaks(res.y[6], prominence = 1.e-6)                          # 1. first scan; detection of significant peaks based on the prominence
                if len(indices)>0 :
                        prominences = peak_prominences(res.y[6], indices)[0]
                        thresh_prom = 0.9*np.max(prominences)
                        indices_max, _ = find_peaks(res.y[6], prominence = thresh_prom)    # 2.a new peak search based on prominence threshold (goal: detect sustained osc)
                        indices_min, _ = find_peaks(-res.y[6], prominence = thresh_prom)   #       (will filter out smaller peaks --> DOES NOT ALWAYS DETECT DAMPED OSC !!!)
                        '''
                        # Test if max in cac are located correctly
                        #prominences = peak_prominences(res.y[6], indices_max)[0]
                        plt.plot(res.t, res.y[6])
                        plt.plot(res.t[indices_max], res.y[6][indices_max], '*r')
                        #plt.vlines(x=res.t[indices_max], ymin=res.y[6][indices_max] - prominences, ymax = res.y[6][indices_max], color = "C1")
                        plt.show()
                        '''
                        #periods = res.t[indices_max[1:]]-res.t[indices_max[0:-1]]
                        #avg_per = np.mean(periods)
                        #print(avg_per, np.std(periods))
                        
                        if len(indices_max) > 1 and len(indices_min) > 1:       # 3.b time series long enough to detect 2 max and 2 min
                                M=min([len(indices_max), len(indices_min)])
                                amplitudes = res.y[6][indices_max[:M]]-res.y[6][indices_min[:M]]
                                if amplitudes[-1] < 0.99*amplitudes[0] or amplitudes[-1] > 1.01*amplitudes[0]:       # 3.c if unstable amplitude --> not yet sustained oscillations
                                        ss = 'no'
                                        ic = 'yes'
                                        first_chunk = 1
                                        #print('Damped oscillations? Not stabilized oscillations?')
                                        # We set first_chunk = 1 because we want to discard the data corresponding to stabilization phase
                                else:                                           # 3.d means stable amplitude --> oscillations
                                        if len(indices_max) < 10:        #need to extend simulation time to have more peaks for further analyses
                                                ss = 'no'
                                                ic = 'yes'
                                                #print('Oscillations.')
                                                regime = 'osc'
                                                delta_total = delta_total + tsaved  # extends simulation time by longer and longer increments
                                                res_tmp.t, res_tmp.y = res_tmp.t[:-1], res_tmp.y[:,:-1]
                                                # We do not set first_chunk = 1 because we want to extend previous data set to obtain more peaks.
                                                # We remove the last point of res_tmp to avoid doublons in data files.
                                        else:
                                                regime = 'osc'
                                                #print('Oscillations -- enough peaks for analysis')
                        else:                   # 4.a means ss not reached
                                ss = 'no'
                                ic = 'yes'
                                first_chunk = 1
                                #print('Ss not reached and peak detected, but no oscillations could be detected.')
                                delta_total = delta_total + 1.5*total  # extends simulation time by 1.5 * total time simulated to try to find another peak (slow osc)
                                # We increase delta_total because we want to generate a longer data set to eventually detect more peaks,
                                # but we also set first_chunk = 1 to discard previous data set in case the peak was part of a transient behaviour.
                else:
            	        ss = 'no'
            	        ic = 'yes'
            	        first_chunk = 1
            	        #print('Ss not reached -- derivatives > 1.e-10. Eventual peaks are artefacts.')
            	        # No peak detected but could be that t
            	        # We set first_chunk = 1 because we want to discard the data corresponding to transient behaviour or numerical instability.
        else:
                #print("Steady state reached -- derivatives < 1.e-10.")
                tsaved = 100.
                NTsaved = 1+int(round(tsaved/dt))
                res.t, res.y = res_tmp.t[-NTsaved:], res_tmp.y[:, -NTsaved:]
                # For steady-state, we only save the last NTsaved points.
                # Useful in the case where "Ss not reached, but no oscillations could be detected", where simulation time was extended beyond tsaved
                # but it is not necessary to save so many points for a SS.
                
#print('Test accoa='+str('%.2e' % (accoa))+' mM has reached steady-state -- tf='+str('%.2f' % tf)+' s.')
NT = len(res.y[0])

flx = np.zeros((NT, 17))
dG = np.zeros((NT, 17))
sigma = np.zeros((17, NT))              # sigma = T*EPR
ncForces = np.zeros((3, NT))            # 0: F_out, 1: F_other, 2: F_Hc
exgCurr = np.zeros((3, NT))             # 0: I_out=IATPc, 1: I_other=IATPc, 2: I_Hc

for i in range(NT):
        flx[i] = fluxes(res.y[:,i])
        dG[i] = deltaG(res.y[:,i])
        
flx = np.transpose(flx)
dG = np.transpose(dG)
sigma = -flx*dG

print(str('ip3=%.6f accoa=%.1e cac=%.4f cam=%.4f caer=%.4f atpc=%.4f atpm=%.4f nadhm=%.4f psi=%.2f oaa=%.2e nadh/nad=%.2e Jtca=%.3e Jo=%.3e total=%.0f %s Jleak=%.3e CIT=%.6f ISOC=%.6f aKG=%.6f SCoA=%.6f SUC=%.6f FUM=%.6f MAL=%.6f OAA=%.2e' % (ip3, accoa, np.mean(res.y[6]), np.mean(res.y[7]), coef1 - np.mean(res.y[6]) * coef2 - np.mean(res.y[7]) * coef3, np.mean(res.y[4]), np.mean(res.y[5]), np.mean(res.y[12]), np.mean(res.y[15]), np.mean(res.y[14]), np.mean(res.y[12]/res.y[13]), np.mean(flx[0]), np.mean(flx[12]), 10000+delta_total, regime, np.mean(vleak*(coef1 - res.y[6] * coef2 - res.y[7] * coef3 - res.y[6]))/uMmM, np.mean(res.y[8]), np.mean(res.y[10]), np.mean(res.y[2]), np.mean(res.y[16]), np.mean(res.y[17]),  np.mean(res.y[9]), np.mean(res.y[11]), np.mean(res.y[14]))))

# Mitochondria
ncForces[0,:] = dG[1,:]+dG[4,:]-3.*dG[6,:]              # ANT + F1 - 3*Hl
ncForces[1,:] = dG[1,:] + (10. * dG[4,:] + 3. * dG[12,:] + dG[2,:] + dG[0,:]+ dG[8,:] + dG[9,:] + dG[15,:] + dG[13,:] + dG[5,:] + dG[10,:]) / 11. - ncForces[0,:]
ncForces[2,:] = (- dG[4,:] + 3. * dG[12,:] + dG[2,:] + dG[0,:] + dG[8,:] + dG[9,:] + dG[15,:] + dG[13,:] + dG[5,:] + dG[10,:]) / 33.

# Redimensionalised with respect to Vm
exgCurr[0,:] = (- flx[7,:])/delta                       # = (- Jhyd)/delta = -Jant at steady state UNCOUPLED
exgCurr[1] = exgCurr[0]                                 # = (- Jhyd)/delta = -Jant at steady state UNCOUPLED
exgCurr[2,:] = 3.*flx[4,:] - 10.*flx[12,:]              # = (3*Jf1 - 10*Jox) = "Jbuffering"/delta - Jhl

res.flux = flx
res.DG = dG
res.EPR = sigma
res.ncW = ncForces * exgCurr

np.save(filename, np.asarray(res))

# Freeing memory
del res_tmp
del res_app
del prt_interval
del res
del dyf
del flx
del dG
del sigma
del ncForces
del exgCurr

gc.collect()
